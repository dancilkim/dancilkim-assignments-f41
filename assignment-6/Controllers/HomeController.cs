﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVCForm.Models;
using test.Models;

namespace test.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost] 
        public ActionResult Submit(Employee fc)
        {
            
            ViewBag.FirstName = fc.FirstName;
            ViewBag.MidName = fc.MidName;
            ViewBag.LastName = fc.LastName;
            ViewBag.Age = fc.Age;
            ViewBag.Address = fc.Address;
            ViewBag.Gender = fc.Gender;

            if(fc.Gender == "Male"){

                ViewBag.pretext = "Mr.";
            }
            else
            {
                 ViewBag.pretext = "Ms.";
            }
            


            return View("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        
    }
}
