using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using server.Models;
using server.Services;

namespace server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserManagementController : ControllerBase
    {
        private IUserServices _userServices;
        public UserManagementController(IUserServices userServices )
        {
            _userServices = userServices;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Login([FromBody]User model)
        {
            var user = _userServices.Login(model.uname, model.pword);

            if(user == null)
                return BadRequest(new { message = "Error"});
                
            return Ok(user);
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody]User model)
        {
            var user = _userServices.Register(model);
            // create user
            return Ok();
        }
    }
}
