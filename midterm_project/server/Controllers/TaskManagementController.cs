﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using server.Models;

namespace server.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("[controller]")]
    public class TaskManagementController : ControllerBase
    {
        private DataContext _ctx;
        public static IWebHostEnvironment _environment;

        public TaskManagementController( DataContext ctx, IWebHostEnvironment environment)
        {
            _environment = environment;
            _ctx = ctx;
        }

        [HttpPost()]
        public async Task<TaskModel> NewTask([FromBody]TaskModel task)
        {
            var _currentUser = new Guid (User.Identity.Name);
            task.creator = _currentUser;
            task.Id = Guid.NewGuid();
            task.when = new DateTime();
            _ctx.tasks.Add(task);
            _ctx.SaveChanges();
            return task;
        }
                
        [HttpGet()]
        public IEnumerable getTask(){
             var _currentUser = new Guid (User.Identity.Name);
             var tasks = from q in _ctx.tasks
             where  q.creator == _currentUser
             select q;

            return tasks;
        }

        [HttpPost("{taskid}/task")]
        public async Task<Taskuser> AddTaskUser(Guid taskid, [FromBody]Taskuser taskuser)
        {
            taskuser.Id = Guid.NewGuid();
            taskuser.taskId = taskid;
            _ctx.taskusers.Add(taskuser);
            _ctx.SaveChanges();
            return taskuser;
        }

        
        [HttpPost("/task_image")]
        public async Task<IActionResult> SaveTask(NewTaskModel task)
        {
            // Uploading files
            var fileName = await UploadFiles();

            // Saving data
            task.Image = fileName;
            _ctx.newTask.Add(task);
            _ctx.SaveChanges();

            return task;
        }


        [HttpPost("/image")]
        public async Task<string> UploadFiles([FromForm] Photo objFile)
        {
            if (objFile.files.Length > 0)
            {
                try
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\Upload\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\Upload\\");
                    }
                    using (FileStream fileStream = System.IO.File.Create(_environment.WebRootPath + "\\Upload\\" + objFile.files.FileName))
                    {
                        objFile.files.CopyTo(fileStream);
                        fileStream.Flush();
                        return "\\Upload\\" + objFile.files.FileName;
                    }


                }
                catch (Exception ex)
                {
                    return ex.Message.ToString();

                    throw;
                }
            }
            else
            {
                return "Failed";
            }
        }
    }
}
