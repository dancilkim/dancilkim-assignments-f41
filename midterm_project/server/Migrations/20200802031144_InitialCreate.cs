﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace server.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tasks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    creator = table.Column<Guid>(nullable: false),
                    task = table.Column<string>(nullable: true),
                    desc = table.Column<string>(nullable: true),
                    from = table.Column<DateTime>(nullable: false),
                    to = table.Column<DateTime>(nullable: false),
                    status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tasks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    firstname = table.Column<string>(nullable: true),
                    lastname = table.Column<string>(nullable: true),
                    position = table.Column<string>(nullable: true),
                    uname = table.Column<string>(nullable: true),
                    pword = table.Column<string>(nullable: true),
                    token = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "taskusers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    taskId = table.Column<Guid>(nullable: false),
                    user = table.Column<Guid>(nullable: false),
                    TaskModelId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_taskusers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_taskusers_tasks_TaskModelId",
                        column: x => x.TaskModelId,
                        principalTable: "tasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_taskusers_TaskModelId",
                table: "taskusers",
                column: "TaskModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "taskusers");

            migrationBuilder.DropTable(
                name: "user");

            migrationBuilder.DropTable(
                name: "tasks");
        }
    }
}
