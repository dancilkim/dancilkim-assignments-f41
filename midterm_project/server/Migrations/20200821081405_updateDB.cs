﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace server.Migrations
{
    public partial class updateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_taskusers_tasks_TaskModelId",
                table: "taskusers");

            migrationBuilder.DropIndex(
                name: "IX_taskusers_TaskModelId",
                table: "taskusers");

            migrationBuilder.DropColumn(
                name: "TaskModelId",
                table: "taskusers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "TaskModelId",
                table: "taskusers",
                type: "char(36)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_taskusers_TaskModelId",
                table: "taskusers",
                column: "TaskModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_taskusers_tasks_TaskModelId",
                table: "taskusers",
                column: "TaskModelId",
                principalTable: "tasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
