using System;

namespace server.Models
{
    public class Photo
    {
        public IFormFile files { get; set; }
    }

    public class NewTaskModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
    }
}