using System;

namespace server.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string position { get; set; }
        public string uname { get; set; }
        public string pword { get; set; }
        public string token { get; set; }

    }
}