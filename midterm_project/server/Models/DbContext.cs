
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace server.Models
{
    public class DataContext : DbContext
    {
        protected readonly IConfiguration Configuration;
        public DataContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            var connection = Configuration.GetConnectionString("DefaultConnection");
            options.UseMySql(connection);
        }

        public DbSet<TaskModel> tasks { get; set; }
        public DbSet<User> user { get; set; }
        public DbSet<Taskuser> taskusers { get; set; }
        public DbSet<NewTaskModel> newTask { get; set; }

    }
}