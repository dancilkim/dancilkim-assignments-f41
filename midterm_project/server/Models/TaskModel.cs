using System;
using System.Collections.Generic;

namespace server.Models
{
    public class TaskModel
    {
        public Guid Id { get; set; }
        public Guid creator { get; set; }
        public string task { get; set;}
        public string desc { get; set;}
        public DateTime from { get; set; }
        public DateTime to { get; set;}
        public string status { get; set;}
        public DateTime when { get; set; }
    
    }
}