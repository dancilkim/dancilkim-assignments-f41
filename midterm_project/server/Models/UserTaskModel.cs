using System;

namespace server.Models{
    public class Taskuser
    {
        public Guid Id { get; set; }
        public Guid taskId { get; set; }
        public Guid user { get; set; }
    }
}