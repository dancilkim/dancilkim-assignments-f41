using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using server.Models;
using server.Includes;

namespace server.Services
{
    public interface IUserServices
    {
        User Register(User user);
        User Login(string uname, string pword);
    }

    public class UserServices : IUserServices
    {
        private DataContext _context;
        private readonly AppSettings _appSettings;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public UserServices(DataContext context, IOptions<AppSettings> appSettings, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _appSettings = appSettings.Value;
            _context = context;
        }

        public User Login(string uname, string pword)
        {   
            var user = _context.user.SingleOrDefault(x => x.uname == uname && x.pword == pword);

            if (user == null)
                return null;
                
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.key);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new System.Security.Claims.ClaimsIdentity(new Claim[]{
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.token = tokenHandler.WriteToken(token);

            user.pword = null;
            return user;
        }

        public User Register(User user)
        {
            if (_context.user.Any(x => x.uname == user.uname))
                throw new AppException("Username \"" + user.uname + "\" not available");

            _context.user.Add(user);
            _context.SaveChanges();

            return user;
        }
        
    }
}