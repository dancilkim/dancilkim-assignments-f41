import Vue from 'vue'
import Vuex from 'vuex'
import {userinfo} from './userinfo'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      userinfo
    },
    strict: process.env.DEV
  })

  return Store
}
