import axios from 'axios'

const myToken = JSON.parse(localStorage.getItem('myToken'));
const state =  myToken
    ? { status: { loggedIn: true },  myToken }
    : { status: {}, userToken: myToken };

const actions = {
   login({dispatch, commit}, loginUser){

        axios.post('http://localhost:2800/usermanagement/login', loginUser)
            .then(userData => {
                
                if (userData.data.token) {
                    localStorage.setItem('myToken', JSON.stringify(userData));
                }
                commit('loginSuccessful', userData);    

            })
            .catch(error => {console.log( error.response.request._response ) })
   },

   register({ dispatch, commit }, userRegistration) {
        console.log(userRegistration);
        axios.post('http://localhost:2800/usermanagement/register', userRegistration)
            .then(userData => {
                commit('loginSuccessful', userData);
                alert("Registration Successful")    
            })
            .catch(error => {console.log( error.response.request._response ) })
    },
   
   logoutUser({commit}){
       localStorage.removeItem('myToken');
       commit('logout');
   }
};

const mutations = {
    loginSuccessful(state, userData){
        state.status = { loggedIn: true };
        state.myToken = userData;
    },
    logout(state){
        state.status = {};
        state.myToken = null;
    }
};

const getters = {
    loggedIn: state => state.status.loggedIn
};

export const userinfo = {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};