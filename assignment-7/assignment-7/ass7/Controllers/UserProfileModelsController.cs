﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ass7.Models;

namespace ass7.Controllers
{
    public class UserProfileModelsController : Controller
    {
        private readonly UserContext _context;

        public UserProfileModelsController(UserContext context)
        {
            _context = context;
        }

        // GET: UserProfileModels
        public async Task<IActionResult> Index()
        {
            return View(await _context.UserProfile.ToListAsync());
        }

        // GET: UserProfileModels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userProfileModel = await _context.UserProfile
                .FirstOrDefaultAsync(m => m.id == id);
            if (userProfileModel == null)
            {
                return NotFound();
            }

            return View(userProfileModel);
        }

        // GET: UserProfileModels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: UserProfileModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,first_name,middle_name,last_name,Age")] UserProfileModel userProfileModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(userProfileModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(userProfileModel);
        }

        // GET: UserProfileModels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userProfileModel = await _context.UserProfile.FindAsync(id);
            if (userProfileModel == null)
            {
                return NotFound();
            }
            return View(userProfileModel);
        }

        // POST: UserProfileModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,first_name,middle_name,last_name,Age")] UserProfileModel userProfileModel)
        {
            if (id != userProfileModel.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(userProfileModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserProfileModelExists(userProfileModel.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(userProfileModel);
        }

        // GET: UserProfileModels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var userProfileModel = await _context.UserProfile
                .FirstOrDefaultAsync(m => m.id == id);
            if (userProfileModel == null)
            {
                return NotFound();
            }

            return View(userProfileModel);
        }

        // POST: UserProfileModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var userProfileModel = await _context.UserProfile.FindAsync(id);
            _context.UserProfile.Remove(userProfileModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserProfileModelExists(int id)
        {
            return _context.UserProfile.Any(e => e.id == id);
        }
    }
}
