﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ass7.Models
{
    public class UserProfileModel
    {
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "Name length can't be more than 8.")]
        public string first_name { get; set; }
        [StringLength(50, ErrorMessage = "Name length can't be more than 8.")]
        public string middle_name { get; set; }
        [StringLength(50)]
        [Required]
        public string last_name { get; set; }

        [RegularExpression("([0-9])", ErrorMessage = "Please Enter Valid Age")]
        [Required]
        public int Age { get; set; }
    }
}
