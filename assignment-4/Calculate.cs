﻿using System;
using System.Collections.Generic;  
using System.Linq;  
using System.Text;  
using System.Threading.Tasks; 

namespace Calculate
{
    //Class for Basic Calculation
    public class basicCalculator {

              
            
            public int Add(int num1, int num2){  
                return num1 + num2;  
            }  
        
            
            public int Multiply(int num1, int num2){  
                return num1 * num2;  
            }  
            
            public int Subtract(int num1, int num2){  
                    if (num1 > num2){  
                        return num1 - num2;  
                    }  
        
                    return num2 - num1;  
        
            }  
              
            public float Division(float num1, float num2){  
                return num1 / num2;  
            }  
    }

    
    public class Conversion{

            
            public string DecimalToBinary(int num){
                 
                int arr;   
                string answer ="";
                for(int i=0; num>0; i++)      
                    {      
                    arr=num%2;      
                    num= num/2;    
                    answer += arr.ToString();
                    }      
                    
                             
                return answer;
            }
    } 

    
    public class Scientific{

        
        public int Factorial(int num)
        {
            int i = 1;
            for (int s = 1; s <= num; s++){
               
                i = i * s;
            }
            return i;
        }

        public int Squared(int num){

            return num * num;

        }
        public int Cube(int num){

            return num * num * num;
        }

        public double SquareRoot(double num){

            return Math.Sqrt(num);
        }

        public double RaisePower(int bas, int pow){

            return Math.Pow(bas, pow);
        }




    }  
        
    

}
